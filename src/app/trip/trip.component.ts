import { Component, OnInit,Input } from '@angular/core';
import { Trip } from '../shared/trip.model';
@Component({
  selector: 'app-trip',
  templateUrl: './trip.component.html'
})
export class TripComponent implements OnInit {
  @Input() trip : Trip;
  constructor() { }

  ngOnInit() {
  }

}
