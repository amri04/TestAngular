import { Component } from '@angular/core';
import { Trip } from './shared/trip.model';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  cities : string[]=[];
  trips : Trip[]=[];
  currentTrip : Trip = new Trip(null,null,null,null);
  addTrip(){
    this.trips.push(Object.assign({},this.currentTrip))  //Object.assign(cible,sources) copie l'objet sources dans l'objet cible et le retourne
    /* ou bien
    this.trips.push(new Trip(this.currentTrip.from,this.currentTrip.to,this.currentTrip.time,this.currentTrip.seats)); )
    */
    this.currentTrip={"from":null,"seats":null,"time":null,"to":null};
  }
}
