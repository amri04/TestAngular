export class Trip{
  constructor(
    public from : string,
    public to : string,
    public time : number,
    public seats : number,
  ){}
}
